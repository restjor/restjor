# -*- coding: utf-8 -*-

# módulo com códigos para serem utilizados nos testes.

import os


BASE_DIR = os.path.abspath(os.path.dirname(__file__))
DATA_DIR = os.path.join(BASE_DIR, 'data')


def get_text(name):
    fname = os.path.join(DATA_DIR, name)

    with open(fname, 'r') as fd:
        content = fd.read()

    return content
