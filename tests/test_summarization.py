# -*- coding: utf-8 -*-

import unittest
from restjor import summarization
from tests import utils


class SummarizationTest(unittest.TestCase):
    def setUp(self):
        raw_text = utils.get_text('texto2.txt')
        self.summarizer = summarization.TextSummarizer(raw_text)

    def test_summarize(self):
        self.assertEqual(len(self.summarizer.summarize()), 5)

    def test_pretty_summary(self):
        pretty_summary = self.summarizer.pretty_summary(use_elipsis=True)
        self.assertIn('...', pretty_summary)

    def test_has_good_size(self):
        sent = self.summarizer.decomposed_text[13]
        self.assertTrue(self.summarizer._has_good_size(sent))

        self.summarizer.sentence_min_size = 20
        self.assertFalse(self.summarizer._has_good_size(sent))
