# -*- coding: utf-8 -*-

import unittest
from restjor import decomposition
from tests import utils


class DecompositionTest(unittest.TestCase):
    def setUp(self):
        self.raw_text = utils.get_text('texto1.txt')
        self.quoted_text = utils.get_text('texto3.txt')

        self.text = decomposition.Text(self.raw_text)

    def test_text_paragraphs(self):
        self.assertEqual(len(list(self.text.paragraphs)), 2)

    def test_text_legth(self):
        self.assertEqual(len(self.text), 3)

    def test_sentences_score(self):
        for sentence in self.text:
            self.assertIsNotNone(sentence.score)

    def test_text_getitem(self):
        first = self.text[0]
        self.assertEqual(first, list(self.text.sentences)[0])
        slc = self.text[1:3]
        self.assertEqual(len(slc), 2)

    def test_sentence_index(self):
        sentence = self.text[1]
        index = self.text.index(sentence)

        self.assertEqual(index, 1)

    def test_handle_quotations(self):
        text = decomposition.Text(self.quoted_text)
        self.assertEqual(len(text), 2)
