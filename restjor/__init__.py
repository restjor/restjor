# -*- coding: utf-8 -*-

"""
ResTJor, o RESumidor de Textos JORnalísticos.

O ResTJor é um sumarizador extrativo que usa uma técnica baseada em grafos
para fazer uma análise estatística do texto fonte e a partir desta
análise extrair as sentenças mais relevantes do texto.

Exemplo de uso:

>>> from restjor import summarize_text
>>> texto = 'Um texto qualquer, de preferência bem grande...'
>>> resumo = summarize_text(texto)
"""

from restjor import summarization


def summarize_text(raw_text, sentence_min_size=None, summary_size=0.3,
                   use_elipsis=False):
    """
    Resume um texto.

    :param raw_text: O texto puro a ser resumido.

    :param sentence_min_size: Tamanho mínimo (em caracteres) da
        sentença a ser incluída no resumo. Se ``sentence_min_size``
        for None (padrão), sentenças de qualquer tamanho serão incluídas
        no resumo.

    :param summary_size: Tamanho relativo do sumário. O padrão é de 30%.
        O parâmetro é um float (30% é igual a 0.3).

    :param use_elipsis: Se True, as sentenças extraídas que não
        forem contíguas no texto original serão separadas por elipses.
    """

    summarizer = summarization.TextSummarizer(raw_text)
    return summarizer.pretty_summary(use_elipsis=use_elipsis)
