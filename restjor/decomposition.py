# -*- coding: utf-8 -*-

"""
Módulo responsável por decompor um texto em sentenças e as sentenças
em palavras.

O uso consiste em criar uma instância de Text e a partir desta instância
pode-se trabalhar com o texto decomposto.

>>> from restjor import decomposition
>>> raw_text = 'Um texto qualquer. Desta vez bem pequeno, só de exemplo.'
>>> text = decomposition.Text(raw_text)
>>> for sentence in text:
...     print(sentence.score, sentence.raw_text)
...
0.0 Um texto qualquer.
0.0 Desta vez bem pequeno, só de exemplo.
>>> sent = text[1]
>>> print(sent.raw_text)
Desta vez bem pequeno, só de exemplo.
"""

import re
import math
import nltk


PORTUGUESE_TOKENIZER = nltk.data.load(
    'tokenizers/punkt/portuguese.pickle')

__all__ = ['BaseText', 'Text', 'Paragraph', 'Sentence']

class BaseText:
    def __hash__(self):
        return hash(self.raw_text)

    def __iter__(self):
        return self.sentences

    def __len__(self):
        return len(list(self.sentences))

    def __getitem__(self, key):
        return list(self.sentences)[key]

    def index(self, sentence):
        return list(self.sentences).index(sentence)


class Text(BaseText):
    """
    Classe responsável por decompor um texto.
    """
    def __init__(self, raw_text):
        """
        :param raw_text: O texto puro a ser resumido
        """
        self.raw_text = raw_text
        # Dividimos o texto em parágrafos para lidarmos
        # com intertitulos e frases sem pontuação final.
        self.paragraph_delimiter = '\n'
        self._paragraphs = None
        self._sentences = None
        self._len = None

    @property
    def paragraphs(self):
        if self._paragraphs is not None:
            for p in  self._paragraphs:
                yield p
        else:
            raw_paras = self.raw_text.split(self.paragraph_delimiter)
            gen = (Paragraph(self, p) for p in raw_paras if p)
            self._paragraphs = []
            for p in gen:
                self._paragraphs.append(p)
                yield p

    @property
    def sentences(self):
        if self._sentences is not None:
            for s in self._sentences:
                yield s

        else:
            gen = (s for p in self.paragraphs for s in p.sentences)
            self._sentences = []
            for s in gen:
                self._sentences.append(s)
                yield s


class Paragraph(BaseText):
    """
    Classe representando um parágrafo de um texto. O parágrafo
    é decomposto em sentenças. O tokenizer usado para separar
    as sentenças é o tokenizers/punkt/portuguese.pickle.
    """
    def __init__(self, text, raw_text):
        """
        :param text: Instância de Text à qual este parágrafo pertence.

        :param raw_text: Texto puro do parágrafo.
        """

        self.text = text
        self.raw_text = raw_text
        self._sentences = None
        self._score = None
        self.sentence_tokenizer = PORTUGUESE_TOKENIZER


    @property
    def sentences(self):
        if self._sentences is not None:
            for s in self._sentences:
                yield s

        else:
            gen = (Sentence(self, s) for s in self.sent_tokenize())
            self._sentences = []
            for s in gen:
                self._sentences.append(s)
                yield s

    def sent_tokenize(self):
        """
        Quebra um texto em sentenças mantendo as aspas intactas.
        """

        sents = self.sentence_tokenizer.tokenize(self.raw_text)
        sents = self._handle_quotations(sents)
        return sents

    def _handle_quotations(self, sentences):
        """
        Trata citações como uma só sentença, evitando a perda de sentido
        nas declarações de alguém.
        """

        pat = re.compile('".*?".*?[\.*|\?*|!*]')
        quotations = re.findall(pat, self.raw_text)
        new_sentences = sentences.copy()
        for sentence in sentences:
            for quotation in quotations:
                if sentence in quotation:
                    try:
                        i = new_sentences.index(sentence)
                    except ValueError:  # pragma: no cover
                        continue

                    if quotation not in new_sentences:
                        new_sentences[i] = quotation
                    else:
                        new_sentences.pop(i)
        return new_sentences


class Sentence(BaseText):
    """
    Classe representando uma sentença de um texto.
    A sentença é decomposta em palavras e é atribuida
    uma pontuação por semelhança relativa às outras
    sentenças. Usa-se um removedor de sufixos para
    o cálculo de semelhança.
    """

    def __init__(self, paragraph, raw_text):
        """
        :param paragraph: Instância de Paragraph à qual
            esta sentença pertence.

        :param raw_text: O texto puro da sentença
        """

        self.paragraph = paragraph
        self.raw_text = raw_text.strip()
        self.stemmer = nltk.stem.RSLPStemmer()
        self._words = None
        self._important_words = None
        self._score = None

    def __iter__(self):
        return self.words

    def __len__(self):
        return len(self.words)

    @property
    def words(self):
        """
        Lista de palavras que compõem a sentença.
        """
        if self._words is not None:
            return self._words

        self._words = nltk.word_tokenize(self.raw_text)
        return self._words

    @property
    def important_words(self):
        """
        set() de palavras que serão consideradas no cálculo
        de semelhança. Aqui é usado o RSLP (removedor de sufixos da
        língua portuguesa) para normalizar as palavras antes do
        cálculo de semelhança.
        """
        if self._important_words is not None:
            return self._important_words

        trash = ['.', '!', ',', '?', ':', ';', '(', ')', '[', ']', '-']
        important = set([self.stemmer.stem(w) for w in self.words
                         if w not in trash])
        self._important_words = important
        return self._important_words

    @property
    def score(self):
        """
        Pontuação da sentença baseada na semelhança com as outras
        sentenças do texto.
        """
        if self._score is not None:
            return self._score

        score = 0.0
        for s in self.paragraph.text.sentences:
            if s == self:
                continue

            similarity = self._calculate_similarity(s)
            score += similarity

        self._score = score
        return self._score

    def _calculate_similarity(self, sentence):
        """
        Calcula a semelhança em relação a outra sentença.

        :param sentence: Instância de Sentece com a qual se quer
            fazer a comparação.
        """

        normalization = float(math.log(len(self.important_words))
                              + math.log(len(sentence.important_words)))

        intersect = self.important_words.intersection(sentence.important_words)
        similarity = len(intersect) / normalization

        return similarity
