# -*- coding: utf-8 -*-

"""
Módulo responsável por resumir um texto. Ele se utiliza do módulo
restjor.decomposition para decompor o texto e baseado na pontuação
das sentenças faz a extração das sentenças mais bem pontuadas.

O uso é o seguinte:

>>> from restjor import summarization
>>> from tests import utils
>>> # utils e os textos de teste estão no package tests, disponíveis
>>> # nos fontes do restjor
>>> txt = utils.get_text('texto2.txt')
>>> # Criando uma instância de TextSummarizer com o nosso texto fonte
>>> summarizer = summarization.TextSummarizer(txt)
>>> # O método summarize() retorna uma lista com as principais sentenças
>>> len(summarizer.summarize())
5
>>> # O método pretty_summary() retorna uma string composta pelas sentenças principais
>>> summarizer.pretty_summary(use_elipsis=True)
'Dezenas de veículos foram incendiados em frente a sede do governo da região de Guerrero, no México, em um protesto pelo desaparecimento e morte de 43 estudantes da escola normal rural de Ayotzinapa... O protesto ocorreu após o procurador-geral da República do país, Jesús Murillo Karam, informar que três homens suspeitos de ser integrantes do cartel Guerreros Unidos confessaram ter matado os estudantes e queimado seus corpos. Na saída da cidade, dois ônibus que voltavam à instituição com os alunos foram alvejados por policiais e traficantes do Guerreros Unidos... Segundo a Procuradoria-Geral do México, os detidos não disseram quem levou os estudantes e quem era o mandante da emboscada... Os pais pediram ao governo que prossiga com as buscas e permita a assistência técnica da Comissão Interamericana de Direitos Humanos. '
"""

from restjor import decomposition

# Este módulo é o responsável por resumir um texto. Ele se utiliza
# do módulo restjor.decomposition e baseado na pontuação das sentenças
# faz a extração das sentenças mais bem pontuadas para o resumo.


class TextSummarizer:
    def __init__(self, raw_text, sentence_min_size=None, summary_size=0.3):
        """
        :param raw_text: Texto puro a ser resumido.

        :param sentence_min_size: Tamanho mínimo (em caracteres) da
            sentença a ser incluída no resumo. Se ``sentence_min_size``
            for None (padrão), sentenças de qualquer tamanho serão incluídas
            no resumo.

        :param summary_size: Tamanho relativo do sumário. O padrão é de 30%.
            O parâmetro é um float (30% é igual a 0.3).
        """
        self.raw_text = raw_text
        self.sentence_min_size = sentence_min_size
        self.summary_size = summary_size
        self.decomposed_text = decomposition.Text(self.raw_text)

    def summarize(self):
        """
        Retorna uma lista com as sentenças melhores pontuadas no texto.
        A quantidade é de acordo com self.summary_size.
        """

        # quantidade de sentenças a serem extraídas
        sqtt = int(len(self.decomposed_text) * self.summary_size)

        valid_sents = [s for s in self.decomposed_text
                       if self._has_good_size(s)]

        best_sents = sorted(valid_sents, key=lambda s: s.score,
                            reverse=True)[:sqtt]

        sorted_sents = sorted(
            best_sents, key=lambda s: self.decomposed_text.index(s))

        return sorted_sents

    def pretty_summary(self, use_elipsis=False):
        """
        Retorna uma string com o resumo.

        :param use_elipsis: Se True, as sentenças extraídas que não
            forem contíguas no texto original serão separadas por elipses.
        """
        sorted_sents = self.summarize()
        if not use_elipsis:
            return ' '.join([s.raw_text for s in sorted_sents])

        pretty_summary = ''
        last_index = None
        for s in sorted_sents:
            index = self.decomposed_text.index(s)

            notelipsis = ((not last_index and index == 2) or
                          (last_index and last_index + 1) == index or
                          index + 1 == len(self.decomposed_text))

            end = ' ' if notelipsis else '.. '

            pretty_summary += s.raw_text + end
            last_index = index

        return pretty_summary

    def _has_good_size(self, sentence):
        if self.sentence_min_size is None:
            return True

        return len(sentence.raw_text) >= self.sentence_min_size
