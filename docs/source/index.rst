.. ResTJor documentation master file, created by
   sphinx-quickstart on Tue Nov 25 17:25:29 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bem-vindo à documentação do ResTJor!
====================================

ResTJor, o RESumidor de Textos JORnalísticos é um sumarizador extrativo que
usa uma técnica baseada em grafos para fazer uma análise estatística do texto
fonte e a partir desta análise extrair as sentenças mais relevantes do texto.


Conteúdo:

.. toctree::
   :maxdepth: 1

   install
   apidoc/restjor





Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
