Documentação da API
===================

Módulo restjor
--------------

.. automodule:: restjor
    :members:
    :undoc-members:
    :show-inheritance:


Sub-módulos
-----------

Módulo restjor.decomposition
----------------------------

.. automodule:: restjor.decomposition
    :members:
    :undoc-members:
    :show-inheritance:

Módulo restjor.summarization
----------------------------

.. automodule:: restjor.summarization
    :members:
    :undoc-members:
    :show-inheritance:
