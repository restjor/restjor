Instalação
==========

A instalação do ResTJor é simples, você pode fazê-la via pip ou direto pelo
repositório git.

Via pip
+++++++

Simplesmente execute o comando:

.. code-block:: sh

    $ pip install restjor

E é isso, já está instalado.


Via repositório git
+++++++++++++++++++

Primeiro clone o projeto:

.. code-block:: sh

   $ git clone https://gitorious.org/restjor/restjor.git
   $ cd restjor

Agora, instale as dependências:

.. code-block:: sh

   $ pip install -r requirements.txt

E por fim rode os testes

.. code-block:: sh

   $ python setup.py test --test-suite=tests

Pronto, restjor instalado!
